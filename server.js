'use strict';

var cookies_es = [
"no busques en twitter lo que no se te ha perdido",
"apuesta todo al 42",
"42",
"nunca te rindas, a menos que tus amigos te lo digan",
"esto -> j'm es un elefante",
"La costumbre es la gran guia de la vida humana.",
"pon MTV ya!",
"A buen culo, buen pedo.",
"Todos nacemos originales y morimos copias.",
"olvidala, es una pela",
"eres sexy sin ropa, que no te lo niegue nadie",
"con gran poder viene gran responsabilidad",
"A borracho o mujeriego, no des a guardar dinero.",
"He pasado una tarde maravillosa. Pero no ha sido esta.",
"A bicho que no conozcas, no le pises la cola.",
"tu no puedes manejar la verdad, en serio",
"Las leyes se hacen para quienes no saben romperlas.",
"Share your knowledge. It's a way to achieve immortality.",
"love is only a feeling",
"La vida es lo que nos mata.",
"Si no puedes ser un buen ejemplo, procura ser una advertencia espantosa. -- Catherine Aird --",
"Cuando el dinero habla, todos callan.",
"Nunca es triste la verdad, lo que no tiene es remedio.",
"j'm <- Mira, un elefante!",
"El destino es el que baraja las cartas, pero nosotros somos los que jugamos.",
"invita una birra a tu mejor amiga, seguro va pendiente",
"alguien te ama en secreto, tu verdadero padre",
"(K)",
"Estoy vivo porque no tengo donde caerme muerto.",
"renuncia a tu trabajo, te pagan mal",
"Nunca haga un favor que no le hayan pedido.",
"Envejecer es inevitable, madurar es opcional.",
"bruce willis is dead at the end of sixth sense",
"No olvides que la muerte es el destino de todos.",
"retweetea este mensaje"];


var cookies_fr = ["Profitons des bons moments au lieu de les traquer",
"Ne laisse jamais personne savoir ce que tu penses",
"Tout n'est pas encore perdu.",
"il n'y pas d'idiot en face : c'est un miroir",
"Ferme les yeux, et le monde devient celui que tu veux",
"Votre femme couche avec son prof de gym. Oui, un de plus",
"L'homme  absurde est celui qui ne change jamais",
"Quand je n'ai pas de bleu, je mets  du rouge",
"Tout est bon quand il est excessif",
"Pour savoir se venger, il faut  savoir souffrir",
"l'homme descend du singe, toi tu y remontes.",
"Comment le vent sait-il dans quelle direction  il doit souffler ?",
"Il ne suffit pas de parler, il faut  parler juste",
"Pour la carotte, le lapin est la parfaite incarnation  du Mal"];

var cookies_en =["believe in yourself, thos everyone else if full of crap",
"change your name to shirley",
"As my old master once said: 'sudo apt-get install fortune'.",
"You're not too cool for school.",
"They who can give up essential liberty for temporary safety, deserve neither liberty nor safety.",
"McLovin is a good name (Y)",
"The future is always scary to those who cling to the past.",
"ny view of things that is not strange is false.",
"stop... HAMMER time!",
"Share your knowledge. It's a way to achieve immortality.",
"il n'y pas d'idiot en face : c'est un miroir",
"leave the gun, take the cannoli",
"When nothing goes right, go left",
"Reality is what refuses to go away when I stop believing in it.",
"For every 10 people who are clipping at the branches of evil, you're lucky to find 1 who's hacking at the roots.",
"",
"Never let your sense of morals prevent you from doing what's right.",
"there's no fortune in this cookie! mwahahaha!",
"Your friend is the man who knows all about you, and still likes you.",
"never give up, trust your insticntcs",
"come with me if you want to live",
"Men are born ignorant, not stupid. They are made stupid by education.",
"get an iPhone, you need one",
"your girlfriend is sooo cheating on you!",
"During times of universal deceit, telling the truth becomes a revolutionary act.",
"True friends stab you in the front.",
"42",
"sleep alone, trust me",
"this is a fortune cookie (or not?)",
"It's better to be lucky than smart, but it's easier to be smart twice than lucky twice.",
"touch yourself",
"don't vote for maduro!",
"I tell you: one must still have chaos in one to give birth to a dancing star!",
"I feel much better, now that I've given up hope."];

var express = require('express');
var Twit = require('twit');
var _ = require('underscore')._;
var T = new Twit({
    consumer_key:         'TNIvh8at5GRmrfGMQQTQQ'
  , consumer_secret:      'HCZMTmwarDU7RCkBdOAxWpuRSyoB9zKWYj2yn2cqmw'
  , access_token:         '153221707-yQvb2fNhC525dDm4VWXzWABxAp9TEBTJBcSFk8QV'
  , access_token_secret:  'p0CYyr7FpFOFrGpD4AkRypjNToVUigCz4wm2KNPX1k'
});
/**
 * Main application file
 */

// Default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Application Config
var config = require('./lib/config/config');

var app = express();

// Express settings
require('./lib/config/express')(app);

// Routing
require('./lib/routes')(app);

// Start server
app.listen(config.port, function () {
  console.log('Express server listening on port %d in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;

var ppl_to_reply = [];
var template = "@<%= username %> <%= mensaje %> #fortunecookie";

function getTweets(keyword,number,language){
	T.get('search/tweets', { q: keyword, count: number }, function(err, reply) {
		reply['statuses'].forEach(function(entry){
			ppl_to_reply.push([entry['user']['screen_name'],entry['id_str'],language]);
		});
	})
}

function getRandomTweet(language){
	switch(language)
	{
		case 1: //en
			return cookies_en[_.random(33)];
			break;
		case 2: //es
			return cookies_es[_.random(34)];
			break;
		case 3: //fr
			return cookies_fr[_.random(13)];
			break;	
		default:	
			return cookies_en[_.random(34)];
	}	
}

function postTweet(username,id,language){	
	var tweet = getRandomTweet(language);
	var new_status = _.template(template,{username:username, mensaje:tweet});
	

	T.post('statuses/update', { status: new_status }, function(err, reply) {
		if(err != null){
			console.log(err);
		}
	});
}

setInterval(
	function(){
		console.log(ppl_to_reply);
		if(ppl_to_reply.length > 0) {
			console.log(ppl_to_reply);
			for (var i=0;i<10;i++)
			{
				var ppl_to_reply_temp = ppl_to_reply[_.random(ppl_to_reply.length)];
				if(ppl_to_reply_temp != null){
					postTweet(ppl_to_reply_temp[0],ppl_to_reply_temp[1],ppl_to_reply_temp[2]);
				}
			}
		}
		ppl_to_reply = [];
		getTweets("fortune",10,1); //en
		getTweets("suerte",10,2); //es
		getTweets("bonne chance",10,3); //fr

	},900000);
